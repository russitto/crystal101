require "http/server"

server = HTTP::Server.new do |context|
  #path = context.request.path
  #method = context.request.method
  context.response.content_type = "text/plain"
  #context.response.print "Hello world!"
  context.response << "Hello world!"
end

address = server.bind_tcp "0.0.0.0", 8080
puts "Listening on http://#{address}"
server.listen
