require "./service"

# Static Routing Class
class Routing
  # static service var
  @@serv = Service.new

  # say hi at home
  def self.home
    @@serv.hi
  end

  # revision by config
  def self.revision
    @@serv.revision
  end

  # revision by git
  def self.rev_by_tag
    @@serv.tag
  end
end
