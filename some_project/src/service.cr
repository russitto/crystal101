require "yaml"

# Simple class `Service`
class Service
  # yaml config via shards
  @shard = YAML.parse(File.read("shard.yml"))

  # returns a json string to greet
  def hi : String
    "\"Hello World!\""
  end

  # returns revision via shard config
  def revision : String
    "\"#{@shard["version"]}\""
  end

  # returns revision via git tag
  def tag : String
    stdout = IO::Memory.new
    stderr = IO::Memory.new
    status = Process.run "git", ["describe", "--abbrev=0"], output: stdout, error: stderr
    "\"#{stdout.to_s.strip}\""
  end
end
