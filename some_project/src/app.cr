require "kemal"
require "./routing"

# we can use shard.yml as config repo
# we must use private vars with env vars

# all endpoints return JSON
before_all do |env|
  env.response.content_type = "application/json"
end

get "/" { Routing.home }

get "/revision" { Routing.revision }

get "/tag" { Routing.rev_by_tag }

Kemal.config.powered_by_header = false
Kemal.run
