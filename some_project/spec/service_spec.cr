require "spec"
require "../src/service"

serv = Service.new
describe Service do
  describe ".hi" do
    it "greetings" do
      serv.hi.should eq "\"Hello World!\""
    end
  end

  describe ".revision" do
    it "should give version by shard in format: d.d.d" do
      serv.revision.should match /^"\d+\.\d+\.\d+"$/
    end
  end

  describe ".tag" do
    it "should give version by git in format: d.d.d" do
      serv.tag.should match /^"\d+\.\d+\.\d+"$/
    end
  end
end
